drop procedure if exists temp_REPORT_STAFF(uuid);
create or replace procedure temp_REPORT_STAFF(_sbid uuid, _actdate date) 
as $$
declare
_cmain refcursor;  
_prioritycode varchar(100);
_hid varchar(2000);
_depname varchar(200);
_num int;
_postname varchar(200);
_ratevol decimal(8,3);
_level int;
_postnumber int;
begin

	CREATE OR REPLACE PROCEDURE pg_temp.performdep(_lhid varchar(2000), _ldepname varchar(200), _llevel int, _lpostnumber in out int)
	as
	$body$
	
	declare	
	_csub refcursor; 
	_ccontent refcursor;
	_hid varchar(2000);
	_depname varchar(200);
	_level int;
	_postname varchar(200);
	_rates decimal(8,3);
	begin

       -- Заголовок
       insert into temp_report (header, level)
       values (_ldepname, _llevel);
      
       -- Содержимое
       open _ccontent for
       select "PostName", "RateVol" 
       from temp_main_rates r
       where r.hid  = _lhid
       and "RateVol">0
       order by r."Num";
      
	   loop
		FETCH _ccontent into _postname, _rates;
		IF NOT FOUND THEN EXIT;END IF;

		insert into temp_report (postnumber, name, rates)
		values (_lpostnumber, _postname, _rates);
	
	        _lpostnumber = _lpostnumber+1;
	       
	   end loop;
	  
	   close _ccontent;
      
	   -- Подчиненные подразделения - рекурсия
	   open _csub for 
	   select distinct hid, "DepName", level, "PriorityCode" 
	   from temp_main_rates 
           where POSITION(_lhid IN hid)>0 
	   and level=_llevel+1 
	   order by "PriorityCode";       
	
	   loop

	   	    FETCH _csub into _hid, _depname, _level;
		    IF NOT FOUND THEN EXIT;END IF;
		   
		    call pg_temp.performdep(_hid, _depname, _level, _lpostnumber);
		   
		end loop;
	
	   close _csub;
      
	   -- Итого, категории
       insert into temp_report (header, rates, level)
       select 'Итого по '||_ldepname, sum("RateVol"), _llevel 
       from temp_hids_cats_rates cr
       where cr.hid  = _lhid
       group by 1; 

       -- Итого, категории
       insert into temp_report (name, rates, iscategory)
       select cr."Category", cr."RateVol", 1 
       from temp_hids_cats_rates cr 
       where cr.hid = _lhid
       and trim(cr."Category")<>''
       order by "PriorityCode";
     
	end;
	$body$
	LANGUAGE plpgsql;

	--1) Создаем перечень всех Хидов
	drop table if exists temp_hids;
	
	create temp table temp_hids as
	select s2."PriorityCode", s2.hid, s2.level from
	(
		WITH RECURSIVE r AS (
		
		   SELECT sb.id, sb."Name", sb."Mnemo"::varchar(2000) as hid, lpad(ps."PriorityCode"::varchar(6),6,' ') as "PriorityCode", 0 as level 
		   FROM userdata."ParusBusinessStructuralSubdivision" sb 
		   inner join userdata."ParusYugBusinessGenericPersonnelSubdivision" gs on (gs."StructuralSubdivision_id" = sb.id)
		   inner join userdata."ParusYugBusinessPersonnelSubdivision" ps on (ps."Owner_id" = gs.id)
		   where sb."HigherSubdivision_id"  is null
		   and gs."InputDate"<_actdate
		   and (gs."OutputDate">_actdate or gs."OutputDate" is null)
		
		   UNION
		
		   SELECT sb1.id, sb1."Name", (r.hid||'->'||sb1."Mnemo"||'=')::varchar(2000) as hid, r."PriorityCode"||','||lpad(ps1."PriorityCode"::varchar(6),6,' ') as "PriorityCode", r.level+1 as level
		   FROM userdata."ParusBusinessStructuralSubdivision" sb1
		   inner join userdata."ParusYugBusinessGenericPersonnelSubdivision" gs1 on (gs1."StructuralSubdivision_id" = sb1.id)
		   inner join userdata."ParusYugBusinessPersonnelSubdivision" ps1 on (ps1."Owner_id" = gs1.id)
		   JOIN r ON sb1."HigherSubdivision_id" = r.id
		   where gs1."InputDate"<_actdate
		   and (gs1."OutputDate">_actdate or gs1."OutputDate" is null)
		   
		)
		select "PriorityCode", hid, level from r
	) s2;

	-- Категории в целом
	drop table if exists temp_category_rates;
	
	create temp table temp_category_rates as
	select s1."PriorityCode", s1.hid, s1."Name", sc."Mnemo" as "CategoryOrdMnemo", sc."Name" as "Category", sum(sh."RateVol") as "RateVol" 
	from 
	(
		WITH RECURSIVE r AS (
		
		   SELECT sb.id, sb."Name", sb."Mnemo"::varchar(2000) as hid, lpad(ps."PriorityCode"::varchar(6),6,' ') as "PriorityCode", 0 as level 
		   FROM userdata."ParusBusinessStructuralSubdivision" sb 
		   inner join userdata."ParusYugBusinessGenericPersonnelSubdivision" gs on (gs."StructuralSubdivision_id" = sb.id)
		   inner join userdata."ParusYugBusinessPersonnelSubdivision" ps on (ps."Owner_id" = gs.id)
		   where sb."HigherSubdivision_id" is null
		   and gs."InputDate"<_actdate
		   and (gs."OutputDate">_actdate or gs."OutputDate" is null)
		
		   UNION
		
		   SELECT sb1.id, sb1."Name", (r.hid||'->'||sb1."Mnemo"||'=')::varchar(2000) as hid, r."PriorityCode"||','||lpad(ps1."PriorityCode"::varchar(6),6,' ') as "PriorityCode", r.level+1 as level
		   FROM userdata."ParusBusinessStructuralSubdivision" sb1
		   inner join userdata."ParusYugBusinessGenericPersonnelSubdivision" gs1 on (gs1."StructuralSubdivision_id" = sb1.id)
		   inner join userdata."ParusYugBusinessPersonnelSubdivision" ps1 on (ps1."Owner_id" = gs1.id)
		   JOIN r ON sb1."HigherSubdivision_id" = r.id
		   where gs1."InputDate"<_actdate
		   and (gs1."OutputDate">_actdate or gs1."OutputDate" is null)
		   
		)
		select id, "Name", hid, "PriorityCode" from r order by hid, "PriorityCode"
	) s1
	left join userdata."ParusYugBusinessPersonnelStaffPost" sp on (sp."Subdivision_id" = s1.id and sp."DateFrom"<now() and (sp."DateTo">now() or sp."DateTo" is null))
	left join userdata."ParusYugBusinessPersonnelStaffPostHistory" sh on (
		sh."StaffPost_id" = sp.id
		and sh."DateOfChange"<_actdate 
		and not exists (
		  select 1 from userdata."ParusYugBusinessPersonnelStaffPostHistory" sh1
		  where sh1."StaffPost_id" = sh."StaffPost_id" 
		  and sh1."DateOfChange" > sh."DateOfChange" 
		)
	)
	left join userdata."ParusYugBusinessGenericPersonnelStaffCategory" sc on (sp."StaffCategory_id" = sc.id)
	group by s1."PriorityCode", s1.hid, sc."OrdinalNumber", s1."Name", sc."Mnemo", sc."Name"
	order by s1."PriorityCode", s1.hid, sc."Mnemo"; 
	
	-- Основные данные
	drop table if exists temp_main_rates;
	
	create temp table temp_main_rates as
	select s1.id, s1."PriorityCode", s1.hid, s1."Name" as "DepName",  sp."Num", sh."Name" as "PostName", sh."RateVol", s1.level 
	from 
	(
		WITH RECURSIVE r AS (
		
		   SELECT sb.id, sb."Name", sb."Mnemo"::varchar(2000) as hid, lpad(ps."PriorityCode"::varchar(6),6,' ') as "PriorityCode", 0 as level 
		   FROM userdata."ParusBusinessStructuralSubdivision" sb 
		   inner join userdata."ParusYugBusinessGenericPersonnelSubdivision" gs on (gs."StructuralSubdivision_id" = sb.id)
		   inner join userdata."ParusYugBusinessPersonnelSubdivision" ps on (ps."Owner_id" = gs.id)
		   where sb."HigherSubdivision_id" is null
		   and gs."InputDate"<_actdate
		   and (gs."OutputDate">_actdate or gs."OutputDate" is null)
		
		   UNION
		
		   SELECT sb1.id, sb1."Name", (r.hid||'->'||sb1."Mnemo"||'=')::varchar(2000) as hid, r."PriorityCode"||','||lpad(ps1."PriorityCode"::varchar(6),6,' ') as "PriorityCode", r.level+1 as level
		   FROM userdata."ParusBusinessStructuralSubdivision" sb1
		   inner join userdata."ParusYugBusinessGenericPersonnelSubdivision" gs1 on (gs1."StructuralSubdivision_id" = sb1.id)
		   inner join userdata."ParusYugBusinessPersonnelSubdivision" ps1 on (ps1."Owner_id" = gs1.id)
		   JOIN r ON sb1."HigherSubdivision_id" = r.id
		   where gs1."InputDate"<_actdate
		   and (gs1."OutputDate">_actdate or gs1."OutputDate" is null)
		   
		)
		select id, "Name", hid, "PriorityCode", level from r order by hid, "PriorityCode"
	) s1
	left join userdata."ParusYugBusinessPersonnelStaffPost" sp on (sp."Subdivision_id" = s1.id and sp."DateFrom"<now() and (sp."DateTo">now() or sp."DateTo" is null))
	left join userdata."ParusYugBusinessPersonnelStaffPostHistory" sh on (
		sh."StaffPost_id" = sp.id
		and sh."DateOfChange"<_actdate 
		and not exists (
		  select 1 from userdata."ParusYugBusinessPersonnelStaffPostHistory" sh1
		  where sh1."StaffPost_id" = sh."StaffPost_id" 
		  and sh1."DateOfChange" > sh."DateOfChange" 
		)
	)
	order by s1."PriorityCode", s1.hid, sp."Num", sh."Name";
	
	-- Смешиваем и получаем подитоги по всем видам категорий
	drop table if exists temp_hids_cats_rates;
	
	create temp table temp_hids_cats_rates as
	select h."PriorityCode", h.hid, coalesce(c."CategoryOrdMnemo",'0') as "OrdMnemo", c."Category", sum(c."RateVol") as "RateVol", h.level
	from temp_hids h, temp_category_rates c
	where strpos(c.hid, h.hid)>0
	group by h."PriorityCode", h.hid, c."CategoryOrdMnemo", c."Category", h.level
	order by h."PriorityCode", h.hid, coalesce(c."CategoryOrdMnemo",'0'), c."Category";
	
        drop table if exists temp_report;
	create temp table temp_report (ordnum serial, postnumber int, header varchar(2000), name varchar(200), rates decimal(8,3), iscategory int, level int);

        _postnumber = 1;

	open _cmain FOR select distinct hid, "DepName", "PriorityCode", level 
	from temp_main_rates where id = _sbid  
	order by "PriorityCode";

	loop
	
            FETCH _cmain into _hid, _depname, _prioritycode, _level;
	    IF NOT FOUND THEN EXIT;END IF;
	   
	    call pg_temp.performdep(_hid, _depname, _level, _postnumber);
	   
	end loop;

end;
$$ LANGUAGE plpgsql;

call temp_REPORT_STAFF(null, now());